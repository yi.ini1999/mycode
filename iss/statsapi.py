#!/usr/bin/python3
"""Alta3 Research | RZFeeser
   Accessing Open APIs with Python"""

# standard library
import json

# 3rd party library
import requests

# Define URL as a global constant (this will not change)
statsapi = 'https://statsapi.web.nhl.com/api/v1/teams'

def main():
    """making API requests"""
    
    # Call the web service
    resp = requests.get(statsapi)  # sends an HTTP GET
    
    # strip JSON data off response and convert
    # to python data types
    data = resp.json()
            
    # display our Pythonic data
    print("\n\nConverted Python data")
    print(data)
    
    print('\n\n\nTeam information\n\n\n\n')
    
    Team = data.get('teams') # people is a list of dict
    print(Team) # this is the list of dict

    # for-loop across astros
    # display names of those in space
    for name in Team:
        print("Riding on the ISS is:", name)

if __name__ == '__main__':
    main()

